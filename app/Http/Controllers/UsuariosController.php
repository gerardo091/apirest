<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Clientes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    //mostrar todos los registros
    public function index(Request $request){

        $token = $request->header('Authorization');
        $clientes = Clientes::all();
        $json = array();

        foreach ($clientes as $key => $value) {
           
            if("Basic ".base64_encode($value["id_cliente"]. ":".$value["llave_secreta"]) ==
            $token){
                //$usuarios = Usuarios::all();
                

                if (isset($_GET['page'])) {
                    $usuarios = DB::table('usuarios')
                ->join('clientes', 'usuarios.id_creador', '=', 'clientes.id')
                ->select('usuarios.id', 'usuarios.nombre', 'usuarios.apellidos', 'usuarios.domicilio', 'usuarios.tipo', 'usuarios.id_creador', 'clientes.primer_nombre', 'clientes.primer_apellido')
                ->paginate(5);
                }else{
                    $usuarios = DB::table('usuarios')
                ->join('clientes', 'usuarios.id_creador', '=', 'clientes.id')
                ->select('usuarios.id', 'usuarios.nombre', 'usuarios.apellidos', 'usuarios.domicilio', 'usuarios.tipo', 'usuarios.id_creador', 'clientes.primer_nombre', 'clientes.primer_apellido')
                ->get();
                }
                
        if (!empty($usuarios)) {
           
            
        $json = array(
            "status"=>200,
            "total_registros"=>count($usuarios),
            "detalles"=>$usuarios
        );

        return json_encode($json, true);
        
        }else{
            $json = array(
                "status"=>200,
                "total_registros"=>0,
                "detalles"=>"no hay registros"
            );
            
          }
         
        } 
    }
        
        return json_encode($json, true);
    
    }




    //Crear un registro
    public function store(Request $request){

        $token = $request->header('Authorization');
        $clientes = Clientes::all();
        $json = array();

        foreach ($clientes as $key => $value) {
            if("Basic ".base64_encode($value["id_cliente"]. ":".$value["llave_secreta"]) ==
            $token)
            {
                //recoger datos
                $datos = array("nombre"=>$request->input("nombre"),
                                "apellidos"=>$request->input("apellidos"),
                                "domicilio"=>$request->input("domicilio"),
                                "email"=>$request->input("email"),
                                "imagen"=>$request->input("imagen"),
                                "tipo"=>$request->input("tipo"));
                if(!empty($datos)){
                    $validator = Validator::make($request->all(), [
                        'nombre' => 'required|string|max:255',
                        'apellidos' => 'required|string|max:255',
                        'domicilio' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:usuarios',
                        'imagen' => 'required|string|max:255',
                        'tipo' => 'required|string|max:255'
                    ]);

                        if ($validator->fails()) {
                            $json = array(
                                "status"=>404,
                                "detalle"=>"Registro con errores: falta completar campos o email repetido"
                            );

                            return json_encode($json, true);
                        }else{

                            $usuarios = new Usuarios();
                            $usuarios->nombre = $datos["nombre"];
                            $usuarios->apellidos = $datos["apellidos"];
                            $usuarios->domicilio = $datos["domicilio"];
                            $usuarios->email = $datos["email"];
                            $usuarios->imagen = $datos["imagen"];
                            $usuarios->tipo = $datos["tipo"];
                            $usuarios->id_creador = $value["id"];

                            $usuarios->save();

                            $json = array(
                                "status"=>200,
                                "detalle"=>"Registro exitoso, el usuario se guardo"
                            );


                        }

                }else{
                    $json = array(
                        "status"=>404,
                        "detalle"=>"Los registros no pueden estar vacios"
                    );
                }
              
            }
        }
        return json_encode($json, true);
    }





        //Tomar un registro
        public function show($id, Request $request){

            $token = $request->header('Authorization');
            $clientes = Clientes::all();
            $json = array();
    
            foreach ($clientes as $key => $value) {

                if("Basic ".base64_encode($value["id_cliente"]. ":".$value["llave_secreta"]) ==
                $token)
                {
                    
                $usuarios = Usuarios::where("id",$id)->get();

                if (!empty($usuarios)) {
                   
                    
                $json = array(
                    "status"=>200,
                    "detalles"=>$usuarios
                );
                
                }else{
                    $json = array(
                        "status"=>200,
                        "total_registros"=>0,
                        "detalles"=>"no hay registros"
                    );
                    
                  }
                 
                
                }else{
                    $json = array(
                        "status"=>404,
                        "detalles"=>"no está autorizado para recibir los registros"
                    );
                }

            }

            return json_encode($json, true);
        }


        //editar un registro
        public function update($id, Request $request){

            $token = $request->header('Authorization');
            $clientes = Clientes::all();
            $json = array();
    
            foreach ($clientes as $key => $value) {
                if("Basic ".base64_encode($value["id_cliente"]. ":".$value["llave_secreta"]) ==
                $token)
                {
                    //recoger datos
                    $datos = array("nombre"=>$request->input("nombre"),
                                    "apellidos"=>$request->input("apellidos"),
                                    "domicilio"=>$request->input("domicilio"),
                                    "email"=>$request->input("email"),
                                    "imagen"=>$request->input("imagen"),
                                    "tipo"=>$request->input("tipo"));
                    if(!empty($datos)){
                        $validator = Validator::make($request->all(), [
                            'nombre' => 'required|string|max:255',
                            'apellidos' => 'required|string|max:255',
                            'domicilio' => 'required|string|max:255',
                            'email' => 'required|string|email|max:255',
                            'imagen' => 'required|string|max:255',
                            'tipo' => 'required|string|max:255'
                        ]);
    
                            if ($validator->fails()) {
                                $json = array(
                                    "status"=>404,
                                    "detalle"=>"Registro con errores: falta completar campos o email repetido"
                                );
    
                                return json_encode($json, true);
                            }else{
    
                                $traer_usuario = Usuarios::where("id",$id)->get();

                                if ($value["id"]==$traer_usuario[0]["id_creador"]) {
                                    
                                    $datos = array("nombre"=>$datos["nombre"],
                                                  "apellidos"=>$datos["apellidos"],
                                                   "domicilio"=>$datos["domicilio"],
                                                   "email"=>$datos["email"],
                                                   "imagen"=>$datos["imagen"],
                                                   "tipo"=>$datos["tipo"]   );
                                    $usuarios = Usuarios::where("id", $id)->update($datos);

                                    $json = array(
                                        "status"=>200,
                                        "detalle"=>"Registro exitoso, el usuario se guardo"
                                    );
                                    
                                    return json_encode($json, true); 
                                }else{
                                    $json = array(
                                        "status"=>404,
                                        "detalle"=>"No está autorizado para modificar este curso"
                                    );
                                    
                                    return json_encode($json, true); 

                                }
    
                            }
    
                    }else{
                        $json = array(
                            "status"=>404,
                            "detalle"=>"Los registros no pueden estar vacios"
                        );
                    }
                  
                }
            }
            return json_encode($json, true);
        }












        //eliminar un registro
        public function destroy($id, Request $request){

            $token = $request->header('Authorization');
            $clientes = Clientes::all();
            $json = array();
    
            foreach ($clientes as $key => $value) {
                if("Basic ".base64_encode($value["id_cliente"]. ":".$value["llave_secreta"]) ==
                $token){

                    $validar = Usuarios::where("id", $id)->get();

                    if (count($validar)>0) {
                        
                        if ($value["id"]==$validar[0]["id_creador"]) {
                            
                            $usuarios = Usuarios::where("id", $id)->delete();
                            $json = array(
                                "status"=>200,
                                "detalle"=>"Se ha borrado el usuario con éxito"
                            );
                            
                            return json_encode($json, true); 
                        }else{
                            $json = array(
                                "status"=>404,
                                "detalle"=>"No está autorizado para eliminar este usuario"
                            );
                            
                            return json_encode($json, true);
                        }

                    }else{
                        $json = array(
                            "status"=>404,
                            "detalle"=>"El curso no existe"
                        );
                        
                        return json_encode($json, true); 
                    }

                }
            }   

        }
}